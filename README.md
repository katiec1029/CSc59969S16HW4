Write a Tutorial

Data Visualization Activity

Write a Tutorial

You need to write a tutorial on a data visualization tool. The form of this tutorial will be a web pages which starts with a data set from here:

http://archive.ics.uci.edu/ml/index.html

and walks through how to visualize this data using one of the libraries listed below. The tutorial should link to code where you read in the data. If you are using a javascript library you will probably show how to convert the data to json or csv if it is not in that form to start.

You should produce a basic figure and then have 3 steps making it more and more complex showing how you use basic features. This is a good tutorial but much longer than I am expecting:

http://nbviewer.jupyter.org/github/jrjohansson/scientific-python-lectures/blob/master/Lecture-4-Matplotlib.ipynb

You should also show a more complex figure with explanation which shows some of the more interesting features of the library you chose. Submit the repo where your

Submit the URL for repo where your html tutorial can be found. Also submit the URL where it is posted somewhere with links back to the repository.

Below find a curated list of some reasonably modern visualization tools:

Javascript Tools:

cesium, http://cesiumjs.org/ cytoscape, http://js.cytoscape.org/ datamaps, http://datamaps.github.io/ dc.js, http://dc-js.github.io/dc.js/ dygraphs, http://dygraphs.com/ networkx, http://jsnetworkx.org/ nvd3, http://nvd3.org/ jsxgraph, http://jsxgraph.uni-bayreuth.de/wp/index.html leaflet, https://leafletjs.com polymaps, http://polymaps.org/ tangle, http://worrydream.com/Tangle/guide.html cubismjs, http://square.github.io/cubism/

Python:

bearcart, https://github.com/wrobstory/bearcart

bokeh, http://bokeh.pydata.org/en/latest/

Brunel, https://github.com/Brunel-Visualization/Brunel

cartopy, http://scitools.org.uk/cartopy/

folium, https://github.com/python-visualization/folium

ggplot, http://ggplot.yhathq.com/

gr, http://gr-framework.org/

mpld3, http://mpld3.github.io/

plotly, https://plot.ly/python/

python-nvd3, https://github.com/areski/python-nvd3

seaborn, https://stanford.edu/~mwaskom/software/seaborn/

vincent, https://vincent.readthedocs.org/en/latest/

vizpy, http://vispy.org/gallery.html

R libraries:

choroplethr, https://github.com/trulia/choroplethr

ggplot2, http://ggplot2.org/ch

shiny, http://shiny.rstudio.com/

Misc:

lightning viz, http://lightning-viz.org
